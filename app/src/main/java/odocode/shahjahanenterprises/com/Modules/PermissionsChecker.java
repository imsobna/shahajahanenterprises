package odocode.shahjahanenterprises.com.Modules;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

public class PermissionsChecker {
    final int permission_all = 1;
    private static final int GRANTED = 0;
    private static final int DENIED = 1;
    private static final int BLOCKED_OR_NEVER_ASKED = 2;
    Context context;
    Activity activity;
    String[] permissions;
    String msg;

    public PermissionsChecker(Context context, Activity activity, String[] permissions, String msg) {
        this.context = context;
        this.activity = activity;
        this.permissions = permissions;
        this.msg = msg;
    }

    public int checkPermissions() {
        int val_return = 0;
        int permissionReturn = hasPermission(context, permissions);
        if ((permissionReturn != GRANTED && permissionReturn != BLOCKED_OR_NEVER_ASKED)) {
            val_return = 1;
            Log.d("val_return1", String.valueOf(val_return));
            ActivityCompat.requestPermissions(activity, permissions, permission_all);
        }
        return val_return;
    }

    public void openAppSettings() {
        Toast.makeText(activity, this.msg, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + activity.getPackageName()));
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        }, 3000);

    }

    private int hasPermission(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                Log.d("permisionsss", String.valueOf(ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)));
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
//                    if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
//                        return BLOCKED_OR_NEVER_ASKED;
//                    }
                    return DENIED;
                }
            }
        }
        return GRANTED;
    }
}
