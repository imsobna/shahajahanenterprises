package odocode.shahjahanenterprises.com.Modules;

import android.content.Context;

import odocode.shahjahanenterprises.com.R;


public class CheckFileRef {
    int[] general_array;
    int[] dental_array;
    int[] ent_array;
    int[] orthopedic_array;
    int[] opthalmology_array;

    public CheckFileRef(Context context) {
        general_array = context.getResources().getIntArray(R.array.general_array);
        dental_array = context.getResources().getIntArray(R.array.dental_array);
        ent_array = context.getResources().getIntArray(R.array.ent_array);
        orthopedic_array = context.getResources().getIntArray(R.array.orthopedic_array);
        opthalmology_array = context.getResources().getIntArray(R.array.opthalmology_array);
    }

    public String checkFileRef(int fileRef) {
        String fileName = "";
        boolean matched = false;
        for (int i = 0; i < general_array.length; i++) {
            if (fileRef == general_array[i]) {
                matched = true;
                fileName = "General";
                break;
            }
        }
        if (!matched) {
            for (int i = 0; i < dental_array.length; i++) {
                if (fileRef == dental_array[i]) {
                    matched = true;
                    fileName = "Dental";
                    break;
                }
            }
        }

        if (!matched) {
            for (int i = 0; i < ent_array.length; i++) {
                if (fileRef == ent_array[i]) {
                    matched = true;
                    fileName = "Ent";
                    break;
                }
            }
        }

        if (!matched) {
            for (int i = 0; i < orthopedic_array.length; i++) {
                if (fileRef == orthopedic_array[i]) {
                    matched = true;
                    fileName = "Orthopedic";
                    break;
                }
            }
        }
        if (!matched) {
            for (int i = 0; i < opthalmology_array.length; i++) {
                if (fileRef == opthalmology_array[i]) {
                    matched = true;
                    fileName = "Opthalmology";
                    break;
                }
            }
        }
        return fileName;
    }
}
