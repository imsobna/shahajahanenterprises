package odocode.shahjahanenterprises.com.Modules;

import java.util.Comparator;

public class RefComparator implements Comparator<FileType> {
    @Override
    public int compare(FileType o1, FileType o2) {
        return o1.getCat_ref().compareTo(o2.getCat_ref());
    }
}