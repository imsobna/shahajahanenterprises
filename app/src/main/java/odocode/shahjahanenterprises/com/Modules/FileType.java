package odocode.shahjahanenterprises.com.Modules;

public class FileType {
    private String page_num, cat_ref, description;

    public FileType(String page_num, String cat_ref, String description) {
        this.page_num = page_num;
        this.cat_ref = cat_ref;
        this.description = description;
    }

    public String getPage_num() {
        return page_num;
    }

    public String getCat_ref() {
        return cat_ref;
    }

    public String getDescription() {
        return description;
    }
}