package odocode.shahjahanenterprises.com.Modules;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class NetworkCheck {
    Context context;
    Activity activity;

    public NetworkCheck(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public boolean isInternetAccessible() {
        AsyncTask<String, Void, Boolean> ress = new CheckInternet().execute();
        try {
            return ress.get();
        } catch (InterruptedException e) {
            Log.d("networkerror",e.getMessage());
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.d("networkerrorrrr",e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private boolean isInternet() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Couldn't check internet connection", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e("log_tag", "Couldn't check internet connection", e);
            }
        } else {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Internet not available!", Toast.LENGTH_SHORT).show();
                }
            });
            Log.d("log_tag", "Internet not available!");
        }
        return false;
    }

    private class CheckInternet extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {

            return isInternet();
        }
    }
}
