package odocode.shahjahanenterprises.com.Modules;

import java.util.List;

public class DataCollection {
    public static List<FileType> arrayList;
    public static List<String> arrayListDesc;
    public static List<String> arrayListRef;
    public static List<String> listGeneralDesc;
    public static List<String> listDentalDesc;
    public static List<String> listEntDesc;
    public static List<String> listOpthalmologyDesc;
    public static List<String> listOrthopedicDesc;
    public static List<String> listRetractorsDesc;


    public DataCollection(List<FileType> arrayList, List<String> arrayListDesc, List<String> arrayListRef
            , List<String> listGeneralDesc, List<String> listDentalDesc, List<String> listEntDesc, List<String> listOpthalmologyDesc,
                          List<String> listOrthopedicDesc, List<String> listRetractorsDesc) {

        this.arrayList = arrayList;
        this.arrayListDesc = arrayListDesc;
        this.arrayListRef = arrayListRef;
        this.listGeneralDesc = listGeneralDesc;
        this.listDentalDesc = listDentalDesc;
        this.listEntDesc = listEntDesc;
        this.listOpthalmologyDesc = listOpthalmologyDesc;
        this.listOrthopedicDesc = listOrthopedicDesc;
        this.listRetractorsDesc = listRetractorsDesc;
    }
}
