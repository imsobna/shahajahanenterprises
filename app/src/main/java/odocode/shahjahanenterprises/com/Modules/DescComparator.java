package odocode.shahjahanenterprises.com.Modules;

import java.util.Comparator;

public class DescComparator implements Comparator<FileType> {
    @Override
    public int compare(FileType o1, FileType o2) {
        return o1.getDescription().compareTo(o2.getDescription());
    }
}