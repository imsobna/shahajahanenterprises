package odocode.shahjahanenterprises.com.Modules;

import android.content.Context;
import android.content.res.AssetManager;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class SearchFromExcel {
    Context context;

    List<FileType> arrayList;
    List<String> arrayListDesc;
    List<String> arrayListRef;
    private List<String> listGeneralDesc;
    private List<String> listDentalDesc;
    private List<String> listEntDesc;
    private List<String> listOpthalmologyDesc;
    private List<String> listOrthopedicDesc;
    private List<String> listRetractorsDesc;
    FileType fileType;
    CheckFileRef checkFileRef;
    private String fileName = "";

    public SearchFromExcel(Context context) {
        this.context = context;
        checkFileRef = new CheckFileRef(context);
        arrayListDesc = new ArrayList<>();
        arrayList = new ArrayList<>();
        arrayListRef = new ArrayList<>();
        listGeneralDesc = new ArrayList<>();
        listDentalDesc = new ArrayList<>();
        listEntDesc = new ArrayList<>();
        listOpthalmologyDesc = new ArrayList<>();
        listOrthopedicDesc = new ArrayList<>();
        listRetractorsDesc = new ArrayList<>();

        readExcelFileFromAssets();

    }

    public void readExcelFileFromAssets() {

        AssetManager assetManager;
        assetManager = context.getAssets();
        try {
            InputStream myInput;

            //  Don't forget to Change to your assets folder excel sheet
            myInput = assetManager.open("cat_list.xls");

            // Create a POIFSFileSystem object
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            // Get the first sheet from workbook
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            /** We now need something to iterate through the cells. **/
            Iterator<Row> rowIter = mySheet.rowIterator();
            int inc = 0;
            rowIter.next();
            while (rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                String rowDesc = myRow.getCell(2).toString();
                String rowRef = myRow.getCell(1).toString();
                fileType = new FileType(myRow.getCell(0).toString(), rowRef, rowDesc);
                arrayList.add(fileType);

                arrayListDesc.add(rowDesc);
                arrayListRef.add(rowRef);
                StringTokenizer tokens = new StringTokenizer(myRow.getCell(0).toString(), ".");
                int fileRef = Integer.parseInt(tokens.nextToken());
                fileName = checkFileRef.checkFileRef(fileRef);
                switch (fileName) {
                    case "General":
                        listGeneralDesc.add(rowDesc);
                        listGeneralDesc.add(rowRef);
                        break;
                    case "Dental":
                        listDentalDesc.add(rowDesc);
                        listDentalDesc.add(rowRef);
                        break;
                    case "Ent":
                        listEntDesc.add(rowDesc);
                        listEntDesc.add(rowRef);
                        break;
                    case "Opthalmology":
                        listOpthalmologyDesc.add(rowDesc);
                        listOpthalmologyDesc.add(rowRef);
                        break;
                    case "Orthopedic":
                        listOrthopedicDesc.add(rowDesc);
                        listOrthopedicDesc.add(rowRef);
                        break;
                    case "Retractors":
                        listRetractorsDesc.add(rowDesc);
                        listRetractorsDesc.add(rowRef);
                        break;
                }
            }
            new DataCollection(arrayList, arrayListDesc, arrayListRef,
                    listGeneralDesc, listDentalDesc, listEntDesc, listOpthalmologyDesc,
                    listOrthopedicDesc, listRetractorsDesc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
