package odocode.shahjahanenterprises.com;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import odocode.shahjahanenterprises.com.Modules.CheckFileRef;
import odocode.shahjahanenterprises.com.Modules.DataCollection;
import odocode.shahjahanenterprises.com.Modules.DescComparator;
import odocode.shahjahanenterprises.com.Modules.FileType;
import odocode.shahjahanenterprises.com.Modules.RefComparator;

public class PdfViewer extends AppCompatActivity {

    PDFView pdfView;
    String fileName = "";
    int defaultPage = 1;
    EditText pageCounter;
    Button btnJumpTo;
    int currentPage, pageCount;
    LinearLayout linearLayout;
    ProgressDialog dialog;
    Toolbar toolbar;
    AutoCompleteTextView searchDesc;
    String selectdDesc = "";
    CheckFileRef checkFileRef;
    List<String> searchDescArray, searchRefArray;
    InputMethodManager imm;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        checkFileRef = new CheckFileRef(this);
        toolbar.setTitleTextColor(Color.parseColor("#C60207"));
        dialog = new ProgressDialog(PdfViewer.this);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        searchDesc = (AutoCompleteTextView) findViewById(R.id.searchDesc);
        pdfView = findViewById(R.id.pdfView);
        btnJumpTo = findViewById(R.id.btnJumpTo);
        pageCounter = findViewById(R.id.pageCounter);
        linearLayout = findViewById(R.id.linearLayout);
        fileName = getIntent().getStringExtra("fileName");
        Log.d("fileName", fileName);
        defaultPage = getIntent().getIntExtra("defaultPage", 1) - 1;
        Log.d("fileName", String.valueOf(defaultPage));
        if (!TextUtils.isEmpty(fileName)) {
            getSupportActionBar().setTitle(fileName);
            switch (fileName) {
                case "General":
                    searchDescArray = DataCollection.listGeneralDesc;
                    break;
                case "Dental":
                    searchDescArray = DataCollection.listDentalDesc;
                    break;
                case "Ent":
                    searchDescArray = DataCollection.listEntDesc;
                    break;
                case "Opthalmology":
                    searchDescArray = DataCollection.listOpthalmologyDesc;
                    break;
                case "Orthopedic":
                    searchDescArray = DataCollection.listOrthopedicDesc;
                    break;
                case "Retractors":
                    searchDescArray = DataCollection.listRetractorsDesc;
                    break;
            }
            if (fileName.equals("Ent") || fileName.equals("Dental") || fileName.equals("Opthalmology")) {
                File dir = new File(getFilesDir() + "/pdf", fileName + ".pdf");
                pdfView.fromFile(dir)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(defaultPage)
                        .onLoad(new OnLoadCompleteListener() {
                            @Override
                            public void loadComplete(int nbPages) {
                                dialog.dismiss();
                                addEvent();
                            }
                        })
                        .onError(new OnErrorListener() {
                            @Override
                            public void onError(Throwable t) {
                                Log.e("pdfError",t.getMessage());
                                Toast.makeText(PdfViewer.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        })
                        .onPageError(new OnPageErrorListener() {
                            @Override
                            public void onPageError(int page, Throwable t) {
                                Toast.makeText(PdfViewer.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                        .password(null)
                        .scrollHandle(null)
                        .enableAntialiasing(true)
                        .spacing(0)
                        .onPageChange(new OnPageChangeListener() {
                            @Override
                            public void onPageChanged(int page, int count) {
                                currentPage = page;
                                pageCount = count;
                                pageCounter.setText(currentPage + 1 + " / " + count);
                            }
                        })
                        .load();
            } else {
                pdfView.fromAsset(fileName + ".pdf")
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(defaultPage)
                        .onLoad(new OnLoadCompleteListener() {
                            @Override
                            public void loadComplete(int nbPages) {
                                dialog.dismiss();
                                addEvent();
                            }
                        })
                        .onError(new OnErrorListener() {
                            @Override
                            public void onError(Throwable t) {
                                Toast.makeText(PdfViewer.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        })
                        .onPageError(new OnPageErrorListener() {
                            @Override
                            public void onPageError(int page, Throwable t) {
                                Toast.makeText(PdfViewer.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                        .password(null)
                        .scrollHandle(null)
                        .enableAntialiasing(true)
                        .spacing(0)
                        .onPageChange(new OnPageChangeListener() {
                            @Override
                            public void onPageChanged(int page, int count) {
                                currentPage = page;
                                pageCount = count;
                                pageCounter.setText(currentPage + 1 + " / " + count);
                            }
                        })
                        .load();
            }

            currentPage = pdfView.getCurrentPage();
            pageCount = pdfView.getPageCount();
            btnJumpTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (imm != null && imm.isAcceptingText()) {
                        imm.hideSoftInputFromWindow(pageCounter.getWindowToken(), 0);
                    }
                    StringTokenizer tokens = new StringTokenizer(pageCounter.getText().toString(), " /");
                    int page = Integer.parseInt(tokens.nextToken());
                    if (page > pageCount) {
                        Toast.makeText(PdfViewer.this, "page not found", Toast.LENGTH_SHORT).show();
                    } else
                        pdfView.jumpTo((page - 1), true);
                }
            });


        }
    }

    private void searchDescInArray(String selectdDesc) {
        int index = 0;

        Collections.sort(DataCollection.arrayList, new DescComparator());
        index = Collections.binarySearch(DataCollection.arrayList, new FileType("", "", selectdDesc),
                new DescComparator());
        boolean isPositive = ((index % (index - 0.03125)) * index) / 0.03125 == index;
        if (!isPositive) {
            Collections.sort(DataCollection.arrayList, new RefComparator());
            index = Collections.binarySearch(DataCollection.arrayList, new FileType("", selectdDesc, ""),
                    new RefComparator());
        }
        openPdf(index);
    }

    private void openPdf(int index) {
        StringTokenizer tokens = new StringTokenizer(DataCollection.arrayList.get(index).getPage_num(), ".");
        int fileRef = Integer.parseInt(tokens.nextToken());
        tokens.nextToken();
        int pageRef = Integer.parseInt(tokens.nextToken());
        if (fileName.equals(checkFileRef.checkFileRef(fileRef))) {
            pdfView.jumpTo(pageRef - 1, true);
        } else {
            Toast.makeText(this, "Search not exist in " + fileName, Toast.LENGTH_SHORT).show();
        }
    }

    private void addEvent() {
        ArrayAdapter<String> adapterDesc;
        adapterDesc = new ArrayAdapter<String>(PdfViewer.this,
                android.R.layout.simple_dropdown_item_1line, searchDescArray);
        searchDesc.setAdapter(adapterDesc);
        searchDesc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectdDesc = String.valueOf(parent.getAdapter().getItem(position));
                if (imm != null && imm.isAcceptingText()) {
                    imm.hideSoftInputFromWindow(pageCounter.getWindowToken(), 0);
                }
                searchDescInArray(selectdDesc);
            }
        });
        searchDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 2) {


                }
                selectdDesc = "";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        KeyboardVisibilityEvent.setEventListener(
                PdfViewer.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        StringTokenizer tokens = new StringTokenizer(pageCounter.getText().toString(), " /");
                        int page = Integer.parseInt(tokens.nextToken());
                        if (isOpen) {
                            pageCounter.setText(String.valueOf(page));

                        } else {
                            pageCounter.setText(page + " / " + pageCount);
                            linearLayout.requestFocus();
                        }
                        pageCounter.setSelection(pageCounter.getText().length());
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
