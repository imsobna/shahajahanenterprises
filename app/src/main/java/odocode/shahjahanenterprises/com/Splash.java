package odocode.shahjahanenterprises.com;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import odocode.shahjahanenterprises.com.Modules.NetworkCheck;
import odocode.shahjahanenterprises.com.Modules.PermissionsChecker;
import odocode.shahjahanenterprises.com.Modules.SearchFromExcel;
import odocode.shahjahanenterprises.com.util.Helper;


public class Splash extends AppCompatActivity {
    private static int SPLASH_TIMEOUT = 1000;
    private static final int GRANTED = 0;
    NetworkCheck networkCheck;
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;
    PermissionsChecker permissionsChecker;
    String[] permissions = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE};
    private StorageReference mStorageRef;
    private StorageReference dentalRef;
    private StorageReference entRef;
    private StorageReference opthalmologyRef;
    private StorageReference downloadRef;
    private File dentalFile;
    private File entFile;
    private File opthalmologyFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        networkCheck = new NetworkCheck(this, Splash.this);
        permissionsChecker = new PermissionsChecker(this, Splash.this, permissions, "please allow these permissions");
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Couldn't check internet connection");
        alertDialogBuilder.setCancelable(false);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        dentalRef = mStorageRef.child("Dental.pdf");
        entRef = mStorageRef.child("Ent.pdf");
        opthalmologyRef = mStorageRef.child("Opthalmology.pdf");
        if (permissionsChecker.checkPermissions() == GRANTED) {

            checkNet();

        }
    }

    private void startDownload() {
        dentalFile = new File(getFilesDir() + "/pdf", "Dental.pdf");
        entFile = new File(getFilesDir() + "/pdf", "Ent.pdf");
        opthalmologyFile = new File(getFilesDir() + "/pdf", "Opthalmology.pdf");
        dirCheck();
    }

    private void dirCheck() {
        if (!dentalFile.exists()) {
            downloadInLocalFile("Dental", dentalRef);
            Toast.makeText(this, "Downloading some additional files", Toast.LENGTH_SHORT).show();
        } else if (!entFile.exists()) {
            downloadInLocalFile("Ent", entRef);
            Toast.makeText(this, "Downloading some additional files just a little wait", Toast.LENGTH_SHORT).show();
        } else if (!opthalmologyFile.exists()) {
            downloadInLocalFile("Opthalmology", opthalmologyRef);
            Toast.makeText(this, "Downloading some additional files almost done", Toast.LENGTH_SHORT).show();
        } else {
            if (dentalFile.exists() && entFile.exists() && opthalmologyFile.exists()) {
                loged();
            }
        }
    }

    private void loged() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new SearchFromExcel(Splash.this);
                startActivity(new Intent(Splash.this, Home.class));
                finish();
            }
        }, SPLASH_TIMEOUT);
    }

    private void downloadInLocalFile(final String fileName, StorageReference downloadRef) {
        this.downloadRef = downloadRef;
        final File dir = new File(getFilesDir().getAbsolutePath() + "/pdf");
        final File file = new File(dir, fileName + ".pdf");
        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final FileDownloadTask fileDownloadTask = downloadRef.getFile(file);

        if (Helper.mProgressDialog == null) {
            Helper.initProgressDialog(this);

            Helper.mProgressDialog.show();
        }

        fileDownloadTask.addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                if (dentalFile.exists() && entFile.exists() && opthalmologyFile.exists()) {
                    Helper.dismissProgressDialog();
                    loged();
                } else {
                    dirCheck();
                }
                Log.d("fileName", file.getPath());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Helper.dismissProgressDialog();
            }
        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                Helper.setProgress(progress);
            }
        });
    }

    private void checkNet() {
        if (networkCheck.isInternetAccessible()) {
            startDownload();
        } else {
            showDialog();
        }
    }

    private void showDialog() {
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        int checkVal = 0;
        boolean allPermissionGranted = false;
        if (requestCode == 1) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allPermissionGranted = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        boolean showRationale;
                        showRationale = shouldShowRequestPermissionRationale(permission);
                        if (!showRationale) {
                            checkVal = 1;
                        }
                    }
                } else {
                    allPermissionGranted = true;
                }
            }
            if (checkVal == 1) {
                permissionsChecker.openAppSettings();

            } else if (allPermissionGranted) {
                downloadInLocalFile("Dental", dentalRef);
            }
        }
    }

}

