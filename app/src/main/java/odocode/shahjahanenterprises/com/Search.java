package odocode.shahjahanenterprises.com;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;


import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.Collections;
import java.util.StringTokenizer;

import odocode.shahjahanenterprises.com.Modules.CheckFileRef;
import odocode.shahjahanenterprises.com.Modules.DataCollection;
import odocode.shahjahanenterprises.com.Modules.DescComparator;
import odocode.shahjahanenterprises.com.Modules.FileType;
import odocode.shahjahanenterprises.com.Modules.RefComparator;

public class Search extends AppCompatActivity {
    String selectdDesc = "";
    String selectdRef = "";
    AutoCompleteTextView searchDesc, searchRef;
    Intent intent;
    CheckFileRef checkFileRef;
    Button btnShowPdf;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        checkFileRef = new CheckFileRef(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchDesc = (AutoCompleteTextView) findViewById(R.id.searchDesc);
        searchRef = (AutoCompleteTextView) findViewById(R.id.searchRef);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        btnShowPdf = findViewById(R.id.btnShowPdf);
        intent = new Intent(Search.this, PdfViewer.class);
        ArrayAdapter<String> adapterDesc = new ArrayAdapter<String>(Search.this,
                android.R.layout.simple_dropdown_item_1line, DataCollection.arrayListDesc);
        searchDesc.setAdapter(adapterDesc);

        ArrayAdapter<String> adapterRef = new ArrayAdapter<String>(Search.this,
                android.R.layout.simple_dropdown_item_1line, DataCollection.arrayListRef);
        searchRef.setAdapter(adapterRef);

        searchDesc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectdDesc = String.valueOf(parent.getAdapter().getItem(position));
                selectdRef = "";
                searchRef.setText("");
                Collections.sort(DataCollection.arrayList, new DescComparator());
            }
        });
        searchDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectdDesc = "";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchRef.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectdRef = String.valueOf(parent.getAdapter().getItem(position));
                selectdDesc = "";
                searchDesc.setText("");
                Collections.sort(DataCollection.arrayList, new RefComparator());
            }
        });
        searchRef.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectdRef = "";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnShowPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(selectdDesc) && !TextUtils.isEmpty(selectdRef)) {

                } else {
                    if (!TextUtils.isEmpty(selectdDesc))
                        searchDescInArray(selectdDesc);
                    else if (!TextUtils.isEmpty(selectdRef))
                        searchRefInArray(selectdRef);
                    else
                        Toast.makeText(Search.this, "Please search and select item", Toast.LENGTH_SHORT).show();
                }
            }
        });
        KeyboardVisibilityEvent.setEventListener(
                Search.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        float d = getResources().getDisplayMetrics().density;
                        int margin = (int) (20 * d);
                        Rect r = new Rect();
                        View rootview = Search.this.getWindow().getDecorView(); // this = activity
                        rootview.getWindowVisibleDisplayFrame(r);
                        int keyboardHeight = rootview.getHeight() - r.bottom;
                        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(btnShowPdf.getWidth(), btnShowPdf.getHeight());
                        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                        lp.setMargins(margin, margin, margin, keyboardHeight + margin);
                        btnShowPdf.setLayoutParams(lp);
                    }
                });
    }


    private void searchDescInArray(String selectdDesc) {

        int index = Collections.binarySearch(DataCollection.arrayList, new FileType("", "", selectdDesc),
                new DescComparator());
        openPdf(index);
    }

    private void openPdf(int index) {
        StringTokenizer tokens = new StringTokenizer(DataCollection.arrayList.get(index).getPage_num(), ".");
        int fileRef = Integer.parseInt(tokens.nextToken());
        tokens.nextToken();
        int pageRef = Integer.parseInt(tokens.nextToken());
        if (imm != null && imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(searchDesc.getWindowToken(), 0);
        }
        intent.putExtra("defaultPage", pageRef);
        intent.putExtra("fileName", checkFileRef.checkFileRef(fileRef));
        startActivity(intent);
    }


    private void searchRefInArray(String selectdRef) {

        int index = Collections.binarySearch(DataCollection.arrayList, new FileType("", selectdRef, ""),
                new RefComparator());
        openPdf(index);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}