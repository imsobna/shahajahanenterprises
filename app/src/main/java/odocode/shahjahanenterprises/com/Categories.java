package odocode.shahjahanenterprises.com;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Categories extends AppCompatActivity {
    Button btnDental, btnGeneral, btnOpthalmology, btnArthopedic, btnRetractors, btnEnt;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intent = new Intent(Categories.this, PdfViewer.class);
        registerEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.ic_search:
                startActivity(new Intent(Categories.this, Search.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    private void registerEvents() {
        btnDental = findViewById(R.id.btnDental);
        btnGeneral = findViewById(R.id.btnGeneral);
        btnOpthalmology = findViewById(R.id.btnOpthalmology);
        btnArthopedic = findViewById(R.id.btnArthopedic);
        btnRetractors = findViewById(R.id.btnRetractors);
        btnEnt = findViewById(R.id.btnEnt);
        btnDental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "Dental");
                startActivity(intent);
            }
        });
        btnGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "General");
                startActivity(intent);
            }
        });

        btnOpthalmology.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "Opthalmology");
                startActivity(intent);
            }
        });

        btnArthopedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "Orthopedic");
                startActivity(intent);
            }
        });

        btnRetractors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "Retractors");
                startActivity(intent);
            }
        });

        btnEnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("fileName", "Ent");
                startActivity(intent);
            }
        });
    }
}
