package odocode.shahjahanenterprises.com.util;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import odocode.shahjahanenterprises.com.R;

public class Helper {
    private static Dialog mDialog;
    public static ProgressDialog mProgressDialog;
    static String msg = "Downloading";


    public static void showDialog(Context context) {
        mDialog = new Dialog(context, R.style.NewDialog);
        mDialog.addContentView(
                new ProgressBar(context),
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        );
        mDialog.setCancelable(true);
        mDialog.show();
    }

    public static void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public static void initProgressDialog(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(msg);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }

    public static void setProgress(int i) {
        mProgressDialog.setProgress(i);
        if (msg.equals("Downloading.........."))
            msg = "Downloading";
        msg = msg + ".";
        mProgressDialog.setMessage(msg);
    }

    public static void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
